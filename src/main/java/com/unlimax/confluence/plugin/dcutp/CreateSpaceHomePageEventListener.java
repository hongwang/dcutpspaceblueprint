package com.unlimax.confluence.plugin.dcutp;

/**
 * Created by hong on 2015/8/11.
 */

import com.atlassian.confluence.core.DefaultSaveContext;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.createcontent.api.events.SpaceBlueprintHomePageCreateEvent;
import com.atlassian.confluence.search.lucene.ConfluenceIndexManager;
import com.atlassian.confluence.security.ContentPermission;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.event.api.EventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CreateSpaceHomePageEventListener {
    private static final String DCUTP_SPACE_COMPLETE_KEY = "com.unlimax.confluence.plugin.dcutpbp:dcutp-space-blueprint";
    private final LabelManager labelManager;
    private final PageManager pageManager;
    private final ConfluenceIndexManager indexManager;
    private final UserAccessor userAccessor;
    private I18NBeanFactory i18NBeanFactory;
    private LocaleManager localeManager;

    public CreateSpaceHomePageEventListener(LabelManager labelManager, PageManager pageManager, ConfluenceIndexManager indexManager, UserAccessor userAccessor,I18NBeanFactory i18NBeanFactory, LocaleManager localeManager) {
        this.labelManager = labelManager;
        this.pageManager = pageManager;
        this.indexManager = indexManager;
        this.userAccessor = userAccessor;
        this.i18NBeanFactory = i18NBeanFactory;
        this.localeManager = localeManager;
    }

    @EventListener
    public void onSpaceHomePageCreate(SpaceBlueprintHomePageCreateEvent event) {
        if (!DCUTP_SPACE_COMPLETE_KEY.equals(event.getSpaceBlueprint().getModuleCompleteKey())) {
            return;
        }

        Space space = event.getSpace();
        String members = (String) event.getContext().get("members");
        String[] usernames = members.split(",");
        List usernamesList = Arrays.asList(usernames);
        grantFirstLevelPagePermissionsToTeamMembers(usernamesList, space);

        List<Label> labels = new ArrayList<Label>();
        labels.add(new Label("dcutp"));
        labels.add(new Label("dcutp-space-sample"));

        I18NBean i18NBean = i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.get()));
        Page homepage = space.getHomePage();
        homepage.setTitle(i18NBean.getText("confluence.blueprints.space.dcutp.homepage.name"));
        pageManager.saveContentEntity(homepage, DefaultSaveContext.DEFAULT);

        Page indexpage = homepage.getChildren().get(0);
        List<Long> childrenList = new ArrayList<Long>();

        for  (Page page : indexpage.getChildren())   {
            childrenList.add(page.getId());
        }

        pageManager.setChildPageOrder(indexpage,childrenList);

        Long homePageId = Long.valueOf(space.getHomePage().getId());
        List<Page> pages = this.pageManager.getPages(space, false);
        for (Page page : pages) {
            if (page.getId() != homePageId.longValue()) {
                for (Label label : labels) {
                    labelManager.addLabel(page, label);
                }

            }
        }
        this.indexManager.flushQueue(ConfluenceIndexManager.IndexQueueFlushMode.ENTIRE_QUEUE);

    }

    private void grantFirstLevelPagePermissionsToTeamMembers(List<String> usernames, Space space) {
        ConfluenceUser user;
        List<Page> pages = space.getHomePage().getDescendants();
        I18NBean i18NBean = i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.get()));
        String groups = i18NBean.getText("confluence.blueprints.space.dcutp.restiction.groups.name");
        String[] groupnames = groups.split(",");
        List<String> groupnamesList = Arrays.asList(groupnames);

        for (Page page : pages) {
            for (String username : usernames) {
                user = userAccessor.getUserByName(username);
                if (user != null) {
                    page.addPermission(ContentPermission.createUserPermission(ContentPermission.VIEW_PERMISSION, user));
                }
            }
            for (String groupname : groupnamesList) {
                page.addPermission(ContentPermission.createGroupPermission(ContentPermission.VIEW_PERMISSION, groupname));
            }
        }
    }

}