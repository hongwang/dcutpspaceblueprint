package com.unlimax.confluence.plugin.dcutp;

/**
 * Created by hong on 2015/8/11.
 */
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.plugins.createcontent.api.events.SpaceBlueprintCreateEvent;
import com.atlassian.confluence.plugins.createcontent.impl.SpaceBlueprint;
import com.atlassian.confluence.plugins.ia.SidebarLinkCategory;
import com.atlassian.confluence.plugins.ia.rest.SidebarLinkBean;
import com.atlassian.confluence.plugins.ia.service.SidebarLinkService;
import com.atlassian.confluence.plugins.ia.service.SidebarService;
import com.atlassian.confluence.rpc.NotPermittedException;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.renderer.v2.components.HtmlEscaper;

import java.util.*;

public class CreateSpaceEventListener
{
    private final SidebarService sidebarService;
    private final SidebarLinkService sidebarLinkService;
    private I18NBeanFactory i18NBeanFactory;
    private LocaleManager localeManager;
    private static final String DCUTP_SPACE_COMPLETE_KEY = "com.unlimax.confluence.plugin.dcutpbp:dcutp-space-blueprint";
    public static final int MAXIMUM_TWO_LINES = 14;
    public static final int MINIMUM_TWO_LINES = 6;
    private final EventPublisher eventPublisher;
    private final UserAccessor userAccessor;
    private final SpacePermissionManager spacePermissionManager;

    public CreateSpaceEventListener(SidebarService sidebarService, SidebarLinkService sidebarLinkService, I18NBeanFactory i18NBeanFactory, LocaleManager localeManager,EventPublisher eventPublisher,UserAccessor userAccessor,SpacePermissionManager spacePermissionManager)
    {
        this.sidebarService = sidebarService;
        this.sidebarLinkService = sidebarLinkService;
        this.i18NBeanFactory = i18NBeanFactory;
        this.localeManager = localeManager;
        this.eventPublisher = eventPublisher;
        this.userAccessor = userAccessor;
        this.spacePermissionManager = spacePermissionManager;
    }

    @EventListener
    public void onSpaceBlueprintCreate(SpaceBlueprintCreateEvent event) throws NotPermittedException {
        if (!event.getSpaceBlueprint().getModuleCompleteKey().equals(DCUTP_SPACE_COMPLETE_KEY)) {
            return;
        }

        Map context = event.getContext();
        Space space = event.getSpace();
        context.put("spaceKey", space.getKey());

        I18NBean i18NBean = i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.get()));
        String pageTitle = i18NBean.getText("confluence.blueprints.space.dcutp.making-a-template.name");
        context.put("makingATemplateLink", "<ri:page ri:content-title=\"" + pageTitle + "\" />");

        String spaceKey = space.getKey();

        sidebarService.setOption(spaceKey, "nav-type", "page-tree");
        sidebarService.setOption(spaceKey, "quick-links-state", "hide");
        for (SidebarLinkBean link : sidebarLinkService.getLinksForSpace(SidebarLinkCategory.MAIN, spaceKey, false))
        {
            sidebarLinkService.hide(spaceKey, Integer.valueOf(link.getId()));
        }

        String members = (String)event.getContext().get("members");

        String[] usernames = members.split(",");

        List usernamesList = Arrays.asList(usernames);

        String teamGrid = generateTeamGrid((String[]) usernamesList.toArray(new String[usernamesList.size()]));
        grantPermissionsToTeamMembers(usernamesList, space);
        event.getContext().put("team", teamGrid);

    }
    private void grantPermissionsToTeamMembers(List<String> usernames, Space space)
    {
        ConfluenceUser user;
        for (String username : usernames)
        {
            user = userAccessor.getUserByName(username);
            if (user != null)
            {
//                for (String permission : SpacePermission.GENERIC_SPACE_PERMISSIONS)
                Collection<String> permissions= new ArrayList<String>();

                permissions.add(SpacePermission.VIEWSPACE_PERMISSION);
                permissions.add(SpacePermission.CREATEEDIT_PAGE_PERMISSION);
                permissions.add(SpacePermission.CREATE_ATTACHMENT_PERMISSION);
                permissions.add(SpacePermission.COMMENT_PERMISSION);
                permissions.add(SpacePermission.EDITBLOG_PERMISSION);
                permissions.add(SpacePermission.EXPORT_SPACE_PERMISSION);
                permissions.add(SpacePermission.USE_CONFLUENCE_PERMISSION);
                permissions.add(SpacePermission.SET_PAGE_PERMISSIONS_PERMISSION);
                permissions.add(SpacePermission.UPDATE_USER_STATUS_PERMISSION);
                permissions.add(SpacePermission.VIEW_USER_PROFILES_PERMISSION);

                for (String permission : permissions)
                {
                    spacePermissionManager.savePermission(SpacePermission.createUserSpacePermission(permission, space, user));
                }
            }
        }
    }

    String generateTeamGrid(String[] usernames) {
        StringBuilder sb = new StringBuilder();

        int numUsers = usernames.length;
        int MAX_COL_NUM;
        if (numUsers < 6)
        {
            MAX_COL_NUM = numUsers;
        }
        else
        {
            if (numUsers < 14)
            {
                MAX_COL_NUM = (int)Math.ceil(numUsers / 2.0F);
            }
            else
            {
                MAX_COL_NUM = 7;
            }
        }
        int colIdx = MAX_COL_NUM;

        for (String username : usernames)
        {
            ConfluenceUser user = userAccessor.getUserByName(username);
            if (user == null) {
                continue;
            }
            if (colIdx == MAX_COL_NUM)
            {
                sb.append("<tr>\n");
            }

            fillUserTemplate(sb, user);

            colIdx--;
            if (colIdx != 0)
                continue;
            sb.append("</tr>\n");
            colIdx = MAX_COL_NUM;
        }

        if ((numUsers > 6) && (colIdx < MAX_COL_NUM))
        {
            while (colIdx-- > 0)
            {
                sb.append("<td></td>\n");
            }
            sb.append("</tr>\n");
        }

        return sb.toString();
    }

    static void fillUserTemplate(StringBuilder sb, ConfluenceUser user)
    {
        sb.append("<td><p style=\"text-align: center;\">");

        sb.append(String.format("<ac:structured-macro ac:name=\"profile-picture\"><ac:parameter ac:name=\"User\"><ri:user ri:userkey=\"%1$s\" /></ac:parameter></ac:structured-macro>", new Object[] { HtmlEscaper.escapeAll(user.getKey().getStringValue(), false) }));

        sb.append("</p><p style=\"text-align: center;\">");
        sb.append(String.format("<strong><a href=\"mailto:%1$s\">%2$s</a></strong>", new Object[] { HtmlEscaper.escapeAll(user.getEmail(), false), HtmlEscaper.escapeAll(user.getFullName(), false) }));

        sb.append("</p></td>\n");
    }

    public void destroy()
            throws Exception
    {
        eventPublisher.unregister(this);
    }
}
