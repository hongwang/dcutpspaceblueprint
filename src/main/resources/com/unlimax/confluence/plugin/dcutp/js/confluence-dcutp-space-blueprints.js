AJS.bind("blueprint.wizard-register.ready", function () {
    function submitDocumentationSpace(e, state) {
        state.pageData.ContentPageTitle = state.pageData.name;
        return Confluence.SpaceBlueprint.CommonWizardBindings.submit(e, state);
    }

    function preRenderDocumentationSpace(e, state) {
        state.soyRenderContext['atlToken'] = AJS.Meta.get('atl-token');
        state.soyRenderContext['showSpacePermission'] = false;
    }

    Confluence.Blueprint.setWizard('com.unlimax.confluence.plugin.dcutpbp:dcutp-space-blueprint-item', function(wizard) {
        wizard.on("submit.documentationSpaceId", submitDocumentationSpace);
        wizard.on("pre-render.documentationSpaceId", preRenderDocumentationSpace);
        wizard.on("post-render.documentationSpaceId", Confluence.SpaceBlueprint.CommonWizardBindings.postRender);
    });
});
